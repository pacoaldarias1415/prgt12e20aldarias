package goodthread;

import java.awt.EventQueue;
import java.util.Random;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 * @author pacoaldarias <paco.aldarias@ceedcv.es>
 */
public class WorkerThread extends Thread {

  private DefaultListModel model;
  private Random generator;

  public WorkerThread(DefaultListModel aModel) {
    model = aModel;
    generator = new Random();
  }

  @Override
  public void run() {
    while (true) {

      final Integer i = new Integer(generator.nextInt(10));
      EventQueue.invokeLater(new Runnable() {
        @Override
        public void run() {
          if (model.contains(i)) {
            boolean removeElement = model.removeElement(i);
          } else {
            model.addElement(i);
          }
        }
      });


      try {
        sleep(1000);
      } catch (InterruptedException ex) {
        Logger.getLogger(WorkerThread.class.getName()).log(Level.SEVERE, null, ex);
      }

    }

  }
}
